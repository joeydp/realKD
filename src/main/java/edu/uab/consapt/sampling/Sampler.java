package edu.uab.consapt.sampling;

public interface Sampler<T> {

	public T getNext();

}
