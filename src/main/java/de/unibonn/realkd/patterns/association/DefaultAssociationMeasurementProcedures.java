/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.association;

import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.powerSet;
import static java.lang.Math.max;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.measures.Measures;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.Frequency;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;

/**
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * @version 0.1.2.1
 *
 */
public enum DefaultAssociationMeasurementProcedures implements MeasurementProcedure<Measure,PatternDescriptor> {

	/**
	 * Procedure for computing the area of a pattern. This is defined as: the
	 * true size of the pattern in the data as a combination of the support and
	 * the size of the pattern. This procedure computes support as an auxiliary
	 * measurement.
	 * 
	 */
	AREA(new MeasurementProcedure<Measure,PatternDescriptor>() {
		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return descriptor instanceof LogicalDescriptor;
		}

		@Override
		public Measure getMeasure() {
			return QualityMeasureId.AREA;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {

			LogicalDescriptor logicalDescriptor = (LogicalDescriptor) descriptor;

			double support = (double) logicalDescriptor.supportSet().size();

			double area = support * logicalDescriptor.size();

			List<Measurement> auxiliaryMeasurements = ImmutableList
					.of(Measures.measurement(QualityMeasureId.SUPPORT, support));

			return Measures.measurement(QualityMeasureId.AREA, area, auxiliaryMeasurements);
		}
	}),

	/**
	 * Computes a lift measurement with auxiliary measurements for frequency,
	 * product of individual frequencies, and difference of frequency.
	 * 
	 */
	LIFT(new MeasurementProcedure<Measure,PatternDescriptor>() {

		private double computeProductOfIndividualFrequencies(LogicalDescriptor description) {
			double product = 1.0;
			int populationSize = description.population().size();

			for (Proposition literal : description.elements()) {
				product *= literal.supportCount() / (double) populationSize;
			}

			return product;
		}

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return (descriptor instanceof LogicalDescriptor);
		}

		@Override
		public Measure getMeasure() {
			return QualityMeasureId.LIFT;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {

			LogicalDescriptor logicalDescriptor = (LogicalDescriptor) descriptor;

			double productOfFrequencies = computeProductOfIndividualFrequencies(logicalDescriptor);

			double denominator = Math.pow(2, logicalDescriptor.size() - 2.);

			double frequency = (double) logicalDescriptor.supportSet().size()
					/ logicalDescriptor.population().size();

			double deviationOfFrequency = frequency - productOfFrequencies;

			double lift = deviationOfFrequency / denominator;

			List<Measurement> auxiliaryMeasurements = ImmutableList.of(
					Measures.measurement(Frequency.FREQUENCY, frequency),
					Measures.measurement(QualityMeasureId.EXPECTED_FREQUENCY,
							productOfFrequencies),
					Measures.measurement(QualityMeasureId.FREQUENCY_DEVIATION,
							deviationOfFrequency));

			return Measures.measurement(QualityMeasureId.LIFT, lift, auxiliaryMeasurements);
		}
	}),
	
	/**
	 * Computes the leverage of an itemset.
	 * 
	 * The leverage tests whether an itemset has a higher support than would be expected
	 * under any assumption of independence between subsets.
	 */
	ITEMSET_LEVERAGE(new MeasurementProcedure<Measure,PatternDescriptor>() {
		
		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return (descriptor instanceof LogicalDescriptor);
		}
		
		@Override
		public Measure getMeasure() {
			return QualityMeasureId.ASSOCIATION_LEVERAGE;
		}
		
		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			
			LogicalDescriptor logicalDescriptor = (LogicalDescriptor) descriptor;
			
			Measurement frequencyMeasurement = DefaultFrequencyMeasurementProcedure.INSTANCE.perform(descriptor);
			
			List<Measurement> auxiliaryMeasurements = ImmutableList.of(frequencyMeasurement);

			if(logicalDescriptor.size() == 1) {
				return Measures.measurement(QualityMeasureId.ASSOCIATION_LEVERAGE, 0, auxiliaryMeasurements);
			}

			double maxFrequency = 0;
			
			for(Set<Proposition> setU: powerSet(newHashSet(logicalDescriptor.elements()))) {
				if(setU.size() == 0 || setU.size() > logicalDescriptor.size()/2) {
					continue;
				}
				
				Set<Proposition> setV = newHashSet(logicalDescriptor.elements());
				setV.removeAll(setU);
				
				maxFrequency = max(maxFrequency,
					DefaultFrequencyMeasurementProcedure.INSTANCE.perform(LogicalDescriptors.create(logicalDescriptor.population(), setU)).value()
					* DefaultFrequencyMeasurementProcedure.INSTANCE.perform(LogicalDescriptors.create(logicalDescriptor.population(), setV)).value());
			}
			
			
			double leverage = frequencyMeasurement.value() - maxFrequency;
			
			
			return Measures.measurement(QualityMeasureId.ASSOCIATION_LEVERAGE, leverage, auxiliaryMeasurements);
		}
	});

	private final MeasurementProcedure<Measure,PatternDescriptor> implementation;

	private DefaultAssociationMeasurementProcedures(MeasurementProcedure<Measure,PatternDescriptor> implementation) {
		this.implementation = implementation;
	}

	@Override
	public boolean isApplicable(PatternDescriptor descriptor) {
		return implementation.isApplicable(descriptor);
	}

	@Override
	public Measure getMeasure() {
		return implementation.getMeasure();
	}

	@Override
	public Measurement perform(PatternDescriptor descriptor) {
		return implementation.perform(descriptor);
	}

}
