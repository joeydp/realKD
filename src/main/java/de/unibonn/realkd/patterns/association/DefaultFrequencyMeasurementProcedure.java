/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.association;

import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.measures.Measures;
import de.unibonn.realkd.patterns.Frequency;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * Procedure for computing the frequency of a pattern. This is defined as: the
 * relative occurrence frequency of the pattern in the complete data.
 * 
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.0
 *
 */
public enum DefaultFrequencyMeasurementProcedure implements MeasurementProcedure<Frequency,PatternDescriptor> {

	INSTANCE;

	private DefaultFrequencyMeasurementProcedure() {
		;
	}

	@Override
	public boolean isApplicable(PatternDescriptor descriptor) {
		return LogicalDescriptor.class.isAssignableFrom(descriptor.getClass());
	}

	@Override
	public Frequency getMeasure() {
		return Frequency.FREQUENCY;
	}

	@Override
	public Measurement perform(PatternDescriptor descriptor) {

		LogicalDescriptor logicalDescriptor = (LogicalDescriptor) descriptor;

		double frequency = (double) logicalDescriptor.supportSet().size()
				/ logicalDescriptor.population().size();

		return Measures.measurement(Frequency.FREQUENCY, frequency);
	}

}
