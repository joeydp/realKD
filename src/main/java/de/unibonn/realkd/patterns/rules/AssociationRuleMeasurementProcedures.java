/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.rules;

import java.util.Collection;

import com.google.common.collect.Sets;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.association.DefaultAssociationMeasurementProcedures;
import de.unibonn.realkd.patterns.association.DefaultFrequencyMeasurementProcedure;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;

/**
 * @author Sandy Moens
 *
 * @since 0.4.0
 *
 * @version 0.4.0
 * 
 */
public enum AssociationRuleMeasurementProcedures implements MeasurementProcedure<Measure, PatternDescriptor> {

	FREQUENCY(new LogicalDescriptorConvertingMeasurementProcedure(DefaultFrequencyMeasurementProcedure.INSTANCE)),

	AREA(new LogicalDescriptorConvertingMeasurementProcedure(DefaultAssociationMeasurementProcedures.AREA)),

	LIFT(new LogicalDescriptorConvertingMeasurementProcedure(DefaultAssociationMeasurementProcedures.LIFT));

	private final MeasurementProcedure<? extends Measure, ? super PatternDescriptor> implementation;

	private AssociationRuleMeasurementProcedures(MeasurementProcedure<Measure, PatternDescriptor> implementation) {
		this.implementation = implementation;
	}

	@Override
	public boolean isApplicable(PatternDescriptor descriptor) {
		return implementation.isApplicable(descriptor);
	}

	@Override
	public Measure getMeasure() {
		return implementation.getMeasure();
	}

	@Override
	public Measurement perform(PatternDescriptor descriptor) {
		return implementation.perform(descriptor);
	}

	private static final class LogicalDescriptorConvertingMeasurementProcedure
			implements MeasurementProcedure<Measure, PatternDescriptor> {

		private MeasurementProcedure<? extends Measure, PatternDescriptor> measurementProcedure;

		public LogicalDescriptorConvertingMeasurementProcedure(
				MeasurementProcedure<? extends Measure, PatternDescriptor> measurementProcedure) {
			this.measurementProcedure = measurementProcedure;
		}

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return RuleDescriptor.class.isAssignableFrom(descriptor.getClass());
		}

		@Override
		public Measure getMeasure() {
			return this.measurementProcedure.getMeasure();
		}

		private LogicalDescriptor ruleToLogical(RuleDescriptor descriptor) {
			Collection<Proposition> elements = Sets.newHashSet();
			elements.addAll(descriptor.getAntecedent().elements());
			elements.addAll(descriptor.getConsequent().elements());

			return LogicalDescriptors.create(descriptor.getAntecedent().population(), elements);
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			LogicalDescriptor logicalDescriptor = ruleToLogical((RuleDescriptor) descriptor);

			return this.measurementProcedure.perform(logicalDescriptor);
		}

	}

}
