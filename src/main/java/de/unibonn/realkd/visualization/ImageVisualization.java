/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.visualization;

import java.awt.Graphics2D;

import org.jfree.graphics2d.svg.SVGGraphics2D;


/**
 * Generic interface for drawing visualizations of a certain type of object. The
 * general contract that is applied is that a user of the class should first
 * test if the visualization is applicable. If true then the visualization can
 * be applied using the draw method.
 * 
 * @author Mario Boley
 * @author Sandy Moens
 * @since 0.3.0
 * @version 0.3.0
 */
public interface ImageVisualization<X> extends Visualization<X>{

	/**
	 * Draws a regular visualization of the object.
	 * 
	 * @param object
	 *            the object to be visualized
	 * @param g
	 * 			  the graphics object on which to render.
	 */
	public void draw(X object, Graphics2D g);
	
	@Override
	default String getType() {
		return "Image";
	}
	
	default SVGGraphics2D getImage(X object, int width, int height) {
		SVGGraphics2D g = new SVGGraphics2D(width, height);
		draw(object, g);
		return g;
	}
	
	default SVGGraphics2D getImage(X object) {
		return getImage(object, 600, 400);
	}
	
	@Override
	default SVGGraphics2D getVisualizationData(X object) {
		return getImage(object);
	}
	
}
