/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.beamsearch;

import static java.lang.Math.min;
import static java.util.Collections.sort;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.computations.dag.DagSearch;
import de.unibonn.realkd.patterns.Pattern;

/**
 * <p>
 * Implements beam search for greedily maximizing some target function. Search
 * starts in a provided root search node and then successively refines the nodes
 * in the beam with a redundant refinement operator. An optimistic estimator can
 * prune elements of the beam.
 * </p>
 * 
 * @param R
 *            the result type
 * 
 * @param N
 *            the search node type
 *
 *
 * @author Panagiotis Mandros
 *
 */
public class NewBeamSearch<R extends Pattern<?>, N> extends AbstractMiningAlgorithm<R>
		implements DagSearch<Collection<? extends Pattern<?>>> {

	private static final Logger LOGGER = Logger.getLogger(NewBeamSearch.class.getName());

	private static class BeamNode<N> {
		public final Function<? super N, ? extends N> lastOperatorUsed;
		public final N content;
		public final int depth;

		public double potential;
		public double value;
		public Set<Function<? super N, ? extends N>> active;
		public Set<Function<? super N, ? extends N>> operatorsUsed;

		public BeamNode(N content, Function<? super N, ? extends N> opOperatorUsed,
				Set<Function<? super N, ? extends N>> operatorsUsed, Set<Function<? super N, ? extends N>> active,
				int depth) {
			this.lastOperatorUsed = opOperatorUsed;
			this.active = active;
			this.content = content;
			this.depth = depth;
			this.operatorsUsed = operatorsUsed;
		}

		public BeamNode(N content, Optional<Function<? super N, ? extends N>> opOperatorUsed,
				Set<Function<? super N, ? extends N>> operatorsUsed, Set<Function<? super N, ? extends N>> active,
				double potential, double value, int depth) {
			this(content, opOperatorUsed.orElse(null), operatorsUsed, active, depth);
			this.potential = potential;
			this.value = value;
		}

		public void updateActive() {
			this.active.remove(this.lastOperatorUsed);
		}

		public String toString() {
			return content.toString();
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (!(o instanceof BeamNode)) {
				return false;
			}

			@SuppressWarnings("unchecked")
			BeamNode<N> that = (BeamNode<N>) o;
			return this.content.equals(that.content);
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.content);
		}

	}

	private static <N> BeamNode<N> beamNode(N content, Function<? super N, ? extends N> opOperatorUsed,
			Set<Function<? super N, ? extends N>> operatorsUsed, Set<Function<? super N, ? extends N>> active,
			int depth) {
		Set<Function<? super N, ? extends N>> newActive = new HashSet<>(active);
		Set<Function<? super N, ? extends N>> newOperatorsUsed = new HashSet<>(operatorsUsed);
		newOperatorsUsed.add(opOperatorUsed);
		return new BeamNode<N>(content, opOperatorUsed, newOperatorsUsed, newActive, depth);
	}

	private static <N> BeamNode<N> beamNode(N content, Optional<Function<? super N, ? extends N>> opOperatorUsed,
			Set<Function<? super N, ? extends N>> operatorsUsed, Set<Function<? super N, ? extends N>> active,
			double potential, double value, int depth) {
		return new BeamNode<N>(content, opOperatorUsed, operatorsUsed, active, potential, value, depth);
	}

	private final ToDoubleFunction<? super N> f;

	private final List<ToDoubleFunction<? super N>> oests;

	private final Function<? super N, ? extends R> toPattern;

	private final Set<BeamNode<N>> boundary;

	private final Set<BeamNode<N>> alreadyExplored;

	// the size of the result queue
	private final int numberOfResults;

	private final int beamWidth;

	private final Optional<Integer> depthLimit;

	private PriorityQueue<BeamNode<N>> best;

	private int nodesCreated = 1;

	private int nodesDiscardedPotential = 0;

	private int nodesDiscardedPotentials[];

	private int maxDepth = 0;

	private int maxBoundarySize = 1;

	// depth of the first encounter of the best solution
	private int solutionDepth = 0;

	// stores the best solution so far, to keep track of depth
	private BeamNode<N> bestSolution;

	public NewBeamSearch(Function<? super N, ? extends R> toPattern, Set<Function<? super N, ? extends N>> active,
			N root, ToDoubleFunction<? super N> f, List<ToDoubleFunction<? super N>> oests, int numberOfResults,
			int beamWidth, Optional<Integer> depthLimit) {
		this.f = f;
		this.oests = oests;
		this.boundary = new HashSet<>();
		this.alreadyExplored = new HashSet<>();
		this.best = new PriorityQueue<>((n, m) -> Double.compare(n.value, m.value));
		this.toPattern = toPattern;
		this.numberOfResults = numberOfResults;
		this.beamWidth = beamWidth;
		this.depthLimit = depthLimit;
		this.nodesDiscardedPotentials = new int[oests.size()];

		// initialize search
		BeamNode<N> rootNode = beamNode(root, Optional.empty(), new HashSet<Function<? super N, ? extends N>>(), active,
				Double.MAX_VALUE, f.applyAsDouble(root), 0);

		best.add(rootNode);
		alreadyExplored.add(rootNode);
		bestSolution = rootNode;
		boundary.add(rootNode);
	}

	public NewBeamSearch(Function<? super N, ? extends R> toPattern, Set<Function<? super N, ? extends N>> active,
			N root, ToDoubleFunction<? super N> f, ToDoubleFunction<? super N> oest, int numberOfResults, int beamWidth,
			Optional<Integer> depthLimit) {
		this(toPattern, active, root, f, ImmutableList.of(oest), numberOfResults, beamWidth, depthLimit);
	}

	public NewBeamSearch(Function<? super N, ? extends R> toPattern, Set<Function<? super N, ? extends N>> active,
			N root, ToDoubleFunction<? super N> f, ToDoubleFunction<? super N> oest) {
		this(toPattern, active, root, f, oest, 1, 1, Optional.empty());
	}

	private void updateResults(BeamNode<N> candidate) {
		/*
		 * if the queue has space insert candidate
		 */
		if (best.size() < numberOfResults) {
			best.add(candidate);
			// check if tracked metrics have to be updated (non crucial for
			// result)
			if (candidate.value > bestSolution.value) {
				bestSolution = candidate;
				solutionDepth = candidate.depth;
				LOGGER.info("Best solution updated: " + bestSolution.content);
			}
		}
		/*
		 * if the queue has no space, and the candidate has bigger value than the least
		 * best current solution, poll the queue, and insert the candidate
		 */
		else if ((candidate.value > best.peek().value) && (best.size() == numberOfResults)) {
			best.poll();
			best.add(candidate);
			if (candidate.value > bestSolution.value) {
				bestSolution = candidate;
				solutionDepth = candidate.depth;
				LOGGER.info("Best solution updated: " + bestSolution.content);

			}
		}
	}

	@Override
	protected Collection<R> concreteCall() {
		while (!stopRequested() && !boundary.isEmpty()) {
			Set<BeamNode<N>> refinements = new HashSet<>();
			for (BeamNode<N> nodeToExpand : boundary) {
				refinements
						.addAll(nodeToExpand.active
								.stream().map(n -> beamNode(n.apply(nodeToExpand.content), n,
										nodeToExpand.operatorsUsed, nodeToExpand.active, nodeToExpand.depth + 1))
								.collect(Collectors.toSet()));
			}
			boundary.clear();
			nodesCreated += refinements.size();

			if (!refinements.isEmpty()) {
				maxDepth++;
			}

			//

			// prune based on rules
			// filterBasedOnAdditionalPruningRules(refinements);

			refinements.forEach(n -> n.value = f.applyAsDouble(n.content));
			// updateResults
			refinements.forEach(this::updateResults);

			// prune and update boundary
			if (!depthLimit.isPresent() || depthLimit.get() > maxDepth) {
				// prune

				List<BeamNode<N>> refinementsToList = new ArrayList<>(refinements);
				// sort and get only the BEAMWIDTH best elements
				refinementsToList.sort((n, m) -> Double.compare(m.value, n.value));
				int sublistBoundary = (beamWidth > refinementsToList.size() ? refinementsToList.size() : beamWidth);
				refinementsToList = refinementsToList.subList(0, sublistBoundary);
				filterBasedOnPotential(refinementsToList);

				// update boundary
				for (BeamNode<N> node : refinementsToList) {
					node.updateActive();
					boundary.add(node);
				}

				trackBoundarySize();
			}
		}

		// logStats();
		List<BeamNode<N>> resultNodes = new ArrayList<>(best);
		sort(resultNodes, (n, m) -> Double.compare(m.value, n.value));
		List<R> result = resultNodes.stream().map(n -> toPattern.apply(n.content)).collect(toList());
		return result;
	}

	// private void filterBasedOnAdditionalPruningRules(List<BeamNode<N>>
	// unevaluatedNodes) {
	// for (int i = unevaluatedNodes.size() - 1; i >= 0; i--) {
	// BeamNode<N> node = unevaluatedNodes.get(i);
	// for (Predicate<N> rule : additionalPruningRules) {
	// if (rule.test(node.content)) {
	// unevaluatedNodes.remove(i);
	// nodesDiscardedPruning++;
	// // only one rule has to apply
	// break;
	// }
	// }
	// }
	// }

	private void trackBoundarySize() {
		if (boundary.size() > maxBoundarySize) {
			maxBoundarySize = boundary.size();
		}
	}

	private void filterBasedOnPotential(List<BeamNode<N>> newNodes) {
		for (int i = newNodes.size() - 1; i >= 0; i--) {
			BeamNode<N> node = newNodes.get(i);
			boolean hasPotential = hasPotential(node);
			if (!hasPotential) {
				newNodes.remove(i);
			}
		}
	}

	private boolean hasPotential(BeamNode<N> candidate) {
		candidate.potential = Double.MAX_VALUE;
		int index = 0;
		for (ToDoubleFunction<? super N> oest : oests) {
			candidate.potential = min(candidate.potential, oest.applyAsDouble(candidate.content));
			if (candidate.potential <= best.peek().value) {
				nodesDiscardedPotentials[index]++;
				nodesDiscardedPotential++;
				return false;
			}
			index++;
		}
		return true;
	}

	@Override
	public String caption() {
		return "";
	}

	@Override
	public String description() {
		return "";
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.OTHER;
	}

	@Override
	public int nodesCreated() {
		return nodesCreated;
	}

	@Override
	public int nodesDiscarded() {
		return nodesDiscardedPotential;
	}

	@Override
	public int boundarySize() {
		return boundary.size();
	}

	@Override
	public int maxAttainedBoundarySize() {
		return maxBoundarySize;
	}

	@Override
	public int maxAttainedDepth() {
		return maxDepth;
	}

	@Override
	public int bestDepth() {
		return solutionDepth;
	}

}
