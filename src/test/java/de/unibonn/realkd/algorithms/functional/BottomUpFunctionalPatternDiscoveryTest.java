//<<<<<<< HEAD
/////*
//// * The MIT License (MIT)
//// *
//// * Copyright (c) 2014-16 The Contributors of the realKD Project
//// *
//// * Permission is hereby granted, free of charge, to any person obtaining a copy
//// * of this software and associated documentation files (the "Software"), to deal
//// * in the Software without restriction, including without limitation the rights
//// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//// * copies of the Software, and to permit persons to whom the Software is
//// * furnished to do so, subject to the following conditions:
//// *
//// * The above copyright notice and this permission notice shall be included in
//// * all copies or substantial portions of the Software.
//// *
//// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//// * THE SOFTWARE.
//// *
//// */
////package de.unibonn.realkd.algorithms.functional;
////
////import static de.unibonn.realkd.data.table.DataTables.table;
////import static org.junit.Assert.assertEquals;
////
////import java.util.ArrayList;
////import java.util.Collection;
////
////import org.junit.Test;
////
////import com.google.common.collect.ImmutableList;
////import com.google.common.collect.ImmutableSet;
////
////import de.unibonn.realkd.common.workspace.Workspace;
////import de.unibonn.realkd.common.workspace.Workspaces;
////import de.unibonn.realkd.data.Population;
////import de.unibonn.realkd.data.Populations;
////import de.unibonn.realkd.data.table.DataTable;
////import de.unibonn.realkd.data.table.attribute.Attributes;
////import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
////import de.unibonn.realkd.data.table.attribute.MetricAttribute;
////import de.unibonn.realkd.patterns.Pattern;
////import de.unibonn.realkd.patterns.functional.CorrelationDescriptor;
////import de.unibonn.realkd.patterns.functional.FractionOfInformation;
////import de.unibonn.realkd.patterns.functional.FunctionalPattern;
////import de.unibonn.realkd.patterns.functional.FunctionalPatterns;
////
/////**
//// * @author Panagiotis Mandros
//// *
//// */
////public class BottomUpFunctionalPatternDiscoveryTest {
////
////	private Population population = Populations.population("anotherTest_population", 6);
////
////	private CategoricAttribute<String> A = Attributes.categoricalAttribute("A", "",
////			ImmutableList.of("1", "1", "1", "1", "2", "2"));
////
////	private CategoricAttribute<String> B = Attributes.categoricalAttribute("B", "",
////			ImmutableList.of("1", "1", "2", "2", "2", "2"));
////
////	private CategoricAttribute<String> C = Attributes.categoricalAttribute("C", "",
////			ImmutableList.of("1", "2", "1", "2", "1", "2"));
////
////	private CategoricAttribute<String> D = Attributes.categoricalAttribute("D", "",
////			ImmutableList.of("2", "1", "2", "1", "2", "1"));
////
////	private CategoricAttribute<String> E = Attributes.categoricalAttribute("E", "",
////			ImmutableList.of("1", "1", "2", "2", "3", "3"));
////
////	private MetricAttribute F = Attributes.metricDoubleAttribute("F", "The black sheep of variables",
////			ImmutableList.of(1.0, 2.0, 3.0, 4.0, 5.0, 6.0));
////
////	private DataTable table = table("table", "table", "", population, ImmutableList.of(A, B, C, D, E, F));
////
////	private Workspace workspace = Workspaces.workspace();
////
////	@Test
////	public void BottomUpThresholdOneTest() {
////		workspace.add(table);
////		GreedyFunctionalPatternDiscovery functionalPatternSearch = new GreedyFunctionalPatternDiscovery(
////				workspace);
////		functionalPatternSearch.fractionOfInformationThreshold(1);
////		functionalPatternSearch.target(E);
////		Collection<Pattern<?>> resultPatterns = functionalPatternSearch.concreteCall();
////
////		CorrelationDescriptor relation1 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(A, B),
////				ImmutableSet.of(E));
////		FunctionalPattern patternA = FunctionalPatterns.functionalPattern(relation1,
////				FractionOfInformation.FRACTION_OF_INFORMATION);
////		// ImmutableSet.copyOf
////		Collection<Pattern<?>> expectedSolution = new ArrayList<>();
////		expectedSolution.add(patternA);
////
////		assertEquals("Result should have 1 pattern", resultPatterns, expectedSolution);
////	}
////
////	@Test
////	public void BottomUpThresholdZeroPointFiveTest() {
////		workspace.add(table);
////		GreedyFunctionalPatternDiscovery functionalPatternSearch = new GreedyFunctionalPatternDiscovery(
////				workspace);
////		functionalPatternSearch.fractionOfInformationThreshold(0.5);
////		functionalPatternSearch.target(E);
////		Collection<Pattern<?>> resultPatterns = functionalPatternSearch.concreteCall();
////
////		CorrelationDescriptor relation1 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(A),
////				ImmutableSet.of(E));
////		FunctionalPattern patternA = FunctionalPatterns.functionalPattern(relation1,
////				FractionOfInformation.FRACTION_OF_INFORMATION);
////
////		CorrelationDescriptor relation2 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(B),
////				ImmutableSet.of(E));
////		FunctionalPattern patternB = FunctionalPatterns.functionalPattern(relation2,
////				FractionOfInformation.FRACTION_OF_INFORMATION);
////
////		Collection<Pattern<?>> expectedSolution = new ArrayList<>();
////		expectedSolution.add(patternA);
////		expectedSolution.add(patternB);
////		assertEquals("Result should have 2 patterns", resultPatterns, expectedSolution);
////	}
////
////	@Test
////	public void BottomUpThresholdZeroTest() {
////		workspace.add(table);
////		GreedyFunctionalPatternDiscovery functionalPatternSearch = new GreedyFunctionalPatternDiscovery(
////				workspace);
////		functionalPatternSearch.fractionOfInformationThreshold(0);
////		functionalPatternSearch.target(E);
////		Collection<Pattern<?>> resultPatterns = functionalPatternSearch.concreteCall();
////
////		CorrelationDescriptor relation1 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(A),
////				ImmutableSet.of(E));
////		FunctionalPattern patternA = FunctionalPatterns.functionalPattern(relation1,
////				FractionOfInformation.FRACTION_OF_INFORMATION);
////
////		CorrelationDescriptor relation2 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(B),
////				ImmutableSet.of(E));
////		FunctionalPattern patternB = FunctionalPatterns.functionalPattern(relation2,
////				FractionOfInformation.FRACTION_OF_INFORMATION);
////
////		CorrelationDescriptor relation3 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(C),
////				ImmutableSet.of(E));
////		FunctionalPattern patternC = FunctionalPatterns.functionalPattern(relation3,
////				FractionOfInformation.FRACTION_OF_INFORMATION);
////
////		CorrelationDescriptor relation4 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(D),
////				ImmutableSet.of(E));
////		FunctionalPattern patternD = FunctionalPatterns.functionalPattern(relation4,
////				FractionOfInformation.FRACTION_OF_INFORMATION);
////
////		Collection<Pattern<?>> expectedSolution = new ArrayList<>();
////		expectedSolution.add(patternA);
////		expectedSolution.add(patternB);
////		expectedSolution.add(patternC);
////		expectedSolution.add(patternD);
////
////		assertEquals("Result should have 4 patterns", resultPatterns, expectedSolution);
////	}
////
////}
//=======
///*
// * The MIT License (MIT)
// *
// * Copyright (c) 2014-16 The Contributors of the realKD Project
// *
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// *
// * The above copyright notice and this permission notice shall be included in
// * all copies or substantial portions of the Software.
// *
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// * THE SOFTWARE.
// *
// */
//package de.unibonn.realkd.algorithms.functional;
//
//import static de.unibonn.realkd.data.table.DataTables.table;
//import static org.junit.Assert.assertEquals;
//
//import java.util.ArrayList;
//import java.util.Collection;
//
//import org.junit.Test;
//
//import com.google.common.collect.ImmutableList;
//import com.google.common.collect.ImmutableSet;
//
//import de.unibonn.realkd.common.workspace.Workspace;
//import de.unibonn.realkd.common.workspace.Workspaces;
//import de.unibonn.realkd.data.Population;
//import de.unibonn.realkd.data.Populations;
//import de.unibonn.realkd.data.table.DataTable;
//import de.unibonn.realkd.data.table.attribute.Attributes;
//import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
//import de.unibonn.realkd.data.table.attribute.MetricAttribute;
//import de.unibonn.realkd.patterns.Pattern;
//import de.unibonn.realkd.patterns.functional.CorrelationDescriptor;
//import de.unibonn.realkd.patterns.functional.FractionOfInformation;
//import de.unibonn.realkd.patterns.functional.FunctionalPattern;
//import de.unibonn.realkd.patterns.functional.FunctionalPatterns;
//
///**
// * @author Panagiotis Mandros
// *
// */
//public class BottomUpFunctionalPatternDiscoveryTest {
//
//	private Population population = Populations.population("anotherTest_population", 6);
//
//	private CategoricAttribute<String> A = Attributes.categoricalAttribute("A", "",
//			ImmutableList.of("1", "1", "1", "1", "2", "2"));
//
//	private CategoricAttribute<String> B = Attributes.categoricalAttribute("B", "",
//			ImmutableList.of("1", "1", "2", "2", "2", "2"));
//
//	private CategoricAttribute<String> C = Attributes.categoricalAttribute("C", "",
//			ImmutableList.of("1", "2", "1", "2", "1", "2"));
//
//	private CategoricAttribute<String> D = Attributes.categoricalAttribute("D", "",
//			ImmutableList.of("2", "1", "2", "1", "2", "1"));
//
//	private CategoricAttribute<String> E = Attributes.categoricalAttribute("E", "",
//			ImmutableList.of("1", "1", "2", "2", "3", "3"));
//
//	private MetricAttribute F = Attributes.metricDoubleAttribute("F", "The black sheep of variables",
//			ImmutableList.of(1.0, 2.0, 3.0, 4.0, 5.0, 6.0));
//
//	private DataTable table = table("table", "table", "", population, ImmutableList.of(A, B, C, D, E, F));
//
//	private Workspace workspace = Workspaces.workspace();
//
//	@Test
//	public void BottomUpThresholdOneTest() {
//		workspace.add(table);
//		BottomUpFunctionalPatternSearchWithSupportPruning functionalPatternSearch = new BottomUpFunctionalPatternSearchWithSupportPruning(
//				workspace);
//		functionalPatternSearch.fractionOfInformationThreshold(1);
//		functionalPatternSearch.target(E);
//		Collection<? extends Pattern<?>> resultPatterns = functionalPatternSearch.concreteCall();
//
//		CorrelationDescriptor relation1 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(A, B),
//				ImmutableSet.of(E));
//		FunctionalPattern patternA = FunctionalPatterns.functionalPattern(relation1,
//				FractionOfInformation.FRACTION_OF_INFORMATION);
//		// ImmutableSet.copyOf
//		Collection<Pattern<?>> expectedSolution = new ArrayList<>();
//		expectedSolution.add(patternA);
//
//		assertEquals("Result should have 1 pattern", resultPatterns, expectedSolution);
//	}
//
//	@Test
//	public void BottomUpThresholdZeroPointFiveTest() {
//		workspace.add(table);
//		BottomUpFunctionalPatternSearchWithSupportPruning functionalPatternSearch = new BottomUpFunctionalPatternSearchWithSupportPruning(
//				workspace);
//		functionalPatternSearch.fractionOfInformationThreshold(0.5);
//		functionalPatternSearch.target(E);
//		Collection<? extends Pattern<?>> resultPatterns = functionalPatternSearch.concreteCall();
//
//		CorrelationDescriptor relation1 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(A),
//				ImmutableSet.of(E));
//		FunctionalPattern patternA = FunctionalPatterns.functionalPattern(relation1,
//				FractionOfInformation.FRACTION_OF_INFORMATION);
//
//		CorrelationDescriptor relation2 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(B),
//				ImmutableSet.of(E));
//		FunctionalPattern patternB = FunctionalPatterns.functionalPattern(relation2,
//				FractionOfInformation.FRACTION_OF_INFORMATION);
//
//		Collection<Pattern<?>> expectedSolution = new ArrayList<>();
//		expectedSolution.add(patternA);
//		expectedSolution.add(patternB);
//		assertEquals("Result should have 2 patterns", resultPatterns, expectedSolution);
//	}
//
//	@Test
//	public void BottomUpThresholdZeroTest() {
//		workspace.add(table);
//		BottomUpFunctionalPatternSearchWithSupportPruning functionalPatternSearch = new BottomUpFunctionalPatternSearchWithSupportPruning(
//				workspace);
//		functionalPatternSearch.fractionOfInformationThreshold(0);
//		functionalPatternSearch.target(E);
//		Collection<? extends Pattern<?>> resultPatterns = functionalPatternSearch.concreteCall();
//
//		CorrelationDescriptor relation1 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(A),
//				ImmutableSet.of(E));
//		FunctionalPattern patternA = FunctionalPatterns.functionalPattern(relation1,
//				FractionOfInformation.FRACTION_OF_INFORMATION);
//
//		CorrelationDescriptor relation2 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(B),
//				ImmutableSet.of(E));
//		FunctionalPattern patternB = FunctionalPatterns.functionalPattern(relation2,
//				FractionOfInformation.FRACTION_OF_INFORMATION);
//
//		CorrelationDescriptor relation3 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(C),
//				ImmutableSet.of(E));
//		FunctionalPattern patternC = FunctionalPatterns.functionalPattern(relation3,
//				FractionOfInformation.FRACTION_OF_INFORMATION);
//
//		CorrelationDescriptor relation4 = FunctionalPatterns.correlationDescriptor(table, ImmutableSet.of(D),
//				ImmutableSet.of(E));
//		FunctionalPattern patternD = FunctionalPatterns.functionalPattern(relation4,
//				FractionOfInformation.FRACTION_OF_INFORMATION);
//
//		Collection<Pattern<?>> expectedSolution = new ArrayList<>();
//		expectedSolution.add(patternA);
//		expectedSolution.add(patternB);
//		expectedSolution.add(patternC);
//		expectedSolution.add(patternD);
//
//		assertEquals("Result should have 4 patterns", resultPatterns, expectedSolution);
//	}
//
//}
//>>>>>>> development
